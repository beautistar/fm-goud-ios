//
//  MenuViewController.swift
//  FM Goud
//
//  Created by Beautistar on 10/15/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit

enum LeftMenu: Int {
    case main = 0
}

protocol LeftMenuProtocol : class {
    func changeViewController(_ menu: LeftMenu)
}

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, LeftMenuProtocol {
    
    var menuNames = [R.menu1]
    @IBOutlet weak var tblMenu: UITableView!
    
    var mainViewController:UIViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.```
    }
    
    func initView() {
        
        tblMenu.tableFooterView = UIView()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)

        let mainTabViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.mainViewController = UINavigationController(rootViewController: mainTabViewController)
    }
    
    /// LeftMenu Protocol
    
    func changeViewController(_ menu: LeftMenu) {
        
        print(menu);
        
        switch menu {
            
        case .main:
            self.perform(#selector(showAlert), with: self, afterDelay: 0.5)
        }
    }
    
    @objc func showAlert() {
        
        let alert = UIAlertController(title: R.name, message: R.alert, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: R.ok, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)        
    }
    
    // TableView datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuNames.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "MenuCell")
        cell.textLabel?.text = R.menu1
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let menu = LeftMenu(rawValue: indexPath.row) {
            self.changeViewController(menu)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
