//
//  ViewController.swift
//  FM Goud
//
//  Created by Beautistar on 10/13/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import UIKit
import QuartzCore
import Jukebox

class ViewController: UIViewController, JukeboxDelegate {
    
    var jukebox : Jukebox!
    var timer : Timer!
    var mainColor = UIColor(red:0.84, green:0.09, blue:0.1, alpha:1)
    
    @IBOutlet weak var statusLabel: MarqueeLabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // begin receiving remote events
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        
        indicator.color = mainColor
        
        // configure jukebox
        jukebox = Jukebox(delegate: self, items: [
            JukeboxItem(URL: URL(string: "http://stream01.level27.be:8008")!),
            JukeboxItem(URL: URL(string: "http://stream01.level27.be:8010")!)
            ])!
        
        /// Later add another item
        //        let delay = DispatchTime.now() + Double(Int64(3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        //        DispatchQueue.main.asyncAfter(deadline: delay) {
        //            self.jukebox.append(item: JukeboxItem (URL: URL(string: "http://www.noiseaddicts.com/samples_1w72b820/2228.mp3")!), loadingAssets: true)
        //        }
        
        statusLabel.tag = 101
        statusLabel.type = .continuous
        statusLabel.animationCurve = .linear
        
        //createMenuView()
  
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        MarqueeLabel.controllerViewWillAppear(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        MarqueeLabel.controllerViewDidAppear(self)
        
        setNavigationBarItem()
    }
    
    @IBAction func goudAction(_ sender: Any) {
        
        jukebox.stop()
        
        self.indicator.alpha = 1
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.hideLoading), userInfo: nil, repeats: false)
        
        switch jukebox.state {
        case .ready :
            jukebox.play(atIndex: 0)
        case .playing :
            if let time = jukebox.currentItem?.currentTime, time > 1.0 || jukebox.playIndex == 1 {
                //jukebox.replayCurrentItem()
                jukebox.playNext()
            } else {
                jukebox.play(atIndex: 0)
            }
        default:
            jukebox.stop()
        }
        
        statusLabel.text = R.channel1_playing + "  " + R.channel1_playing
    }
    

    
    @IBAction func goudPlusAction(_ sender: Any) {
        
        jukebox.stop()
        self.indicator.alpha = 1
        
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.hideLoading), userInfo: nil, repeats: false)
        
        switch jukebox.state {
        case .ready :
            jukebox.play(atIndex: 1)
        case .playing :
            if let time = jukebox.currentItem?.currentTime, time > 1.0 || jukebox.playIndex == 0 {
                jukebox.playPrevious()
            } else {
                jukebox.play(atIndex: 1)
            }
        default:
            jukebox.stop()
        }
        
        statusLabel.text = R.channel2_playing + "  " + R.channel2_playing
    }
    
    @IBAction func stopAction(_ sender: Any) {
        
        jukebox.stop()
        statusLabel.text = R.stop
        exit(0)
    }
    
    @IBAction func menuAction(_ sender: Any) {
        
        let alert = UIAlertController(title: R.name, message: R.alert, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: R.ok, style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @objc func hideLoading() {
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.indicator.alpha = 0
        })
    }
    
    //Jukebox delegate
    
    func jukeboxStateDidChange(_ jukebox: Jukebox) {
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.indicator.alpha = jukebox.state == .loading ? 1 : 0
            //self.indicator.alpha = jukebox.state == .playing ? 1 : 0
            //self.playPauseButton.alpha = jukebox.state == .loading ? 0 : 1
            //self.playPauseButton.isEnabled = jukebox.state == .loading ? false : true
        })
        
        if jukebox.state == .ready {
            //playPauseButton.setImage(UIImage(named: "playBtn"), for: UIControlState())
        } else if jukebox.state == .loading  {
            //playPauseButton.setImage(UIImage(named: "pauseBtn"), for: UIControlState())
        } else {
            //volumeSlider.value = jukebox.volume
            let imageName: String
            switch jukebox.state {
            case .playing, .loading:
                imageName = "pauseBtn"
            case .paused, .failed, .ready:
                imageName = "playBtn"
            }
            
            print(imageName)
            //playPauseButton.setImage(UIImage(named: imageName), for: UIControlState())
        }
        
        print("Jukebox state changed to \(jukebox.state)")
    }
    
    func jukeboxPlaybackProgressDidChange(_ jukebox: Jukebox) {
        if let currentTime = jukebox.currentItem?.currentTime, let duration = jukebox.currentItem?.meta.duration {
            print("progress did changed : current time", currentTime)
            print("progress did changed : duration", duration)
            //let value = Float(currentTime / duration)
            //slider.value = value
            //populateLabelWithTime(currentTimeLabel, time: currentTime)
            //populateLabelWithTime(durationLabel, time: duration)
        } else {
            //resetUI()
        }
    }
    
    func jukeboxDidLoadItem(_ jukebox: Jukebox, item: JukeboxItem) {
        print("Jukebox did load: \(item.URL.lastPathComponent)")
    }
    
    func jukeboxDidUpdateMetadata(_ jukebox: Jukebox, forItem: JukeboxItem) {
        print("Item updated:\n\(forItem)")
    }
    
    override func remoteControlReceived(with event: UIEvent?) {
        if event?.type == .remoteControl {
            switch event!.subtype {
            case .remoteControlPlay :
                jukebox.play()
            case .remoteControlPause :
                jukebox.pause()
            case .remoteControlNextTrack :
                jukebox.playNext()
            case .remoteControlPreviousTrack:
                jukebox.playPrevious()
            case .remoteControlTogglePlayPause:
                if jukebox.state == .playing {
                    jukebox.pause()
                } else {
                    jukebox.play()
                }
            default:
                break
            }
        }
    }
    
}

