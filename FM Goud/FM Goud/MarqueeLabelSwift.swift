//
//  MarqueeLabelSwift.swift
//  FM Goud
//
//  Created by Beautistar on 10/13/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

open class MarqueeLabelSwift: MarqueeLabel {
    // Keeps Interface Builder from getting confused due to the
    // multiple classes named MarqueeLabel in the project file (Swift & Obj-C)
}
