//
//  R.swift
//  FM Goud
//
//  Created by Beautistar on 10/13/17.
//  Copyright © 2017 Beautistar. All rights reserved.
//

import Foundation

struct R {
    
    static let name = "FM Goud"
    
    static let alert = "Met 54.470 regelmatige luisteraars is FM Goud uitgegroeid tot de best beluisterde regionale radiozender van de provincie Limburg. Bij ons hoor je iedere dag opnieuw, 24 uur op 24 en 7 dagen op 7, de beste muziekmix voor Noord-Limburg. Vakkundig aan mekaar gepraat door professionele stemmen, die zelf uit de regio komen. Dagelijks houden wij je ook op de hoogte van de activiteiten die in Noord-Limburg worden georganiseerd en van al het nieuws uit de regio. \n\nwww.fmgoud.be en www.facebook.com/fmgoud"
    static let ok = "OK"
    
    
    static let channel1 = "Luister FM Goud"
    static let channel2 = "Luister FM Goud Plus"
    static let channel1_playing = "Now playing : Luister FM Goud"
    static let channel2_playing = "Now playing : Luister FM Goud Plus"
    static let close = "Sluiten"
    static let stop = "stop"
    static let menu1 = "over ons"
    
    static let channel1_url = "http://stream01.level27.be:8008"
    static let channel2_url = "http://stream01.level27.be:8010"
    
}

